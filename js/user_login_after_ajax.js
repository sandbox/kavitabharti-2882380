(function($, Drupal)
{
	// On page load hide Password field & Submit button from User login page.
	Drupal.behaviors.beforeAjaxCallbackExample = {
		attach: function (context, settings) {
			$('#user-login .form-item-pass').once(function(){
				$( "#user-login .form-item-pass" ).hide();
				$( "#user-login .form-actions" ).hide();
			});
		}
    };
	
	// Showing password field on ajax trigger only if username is valid :
	Drupal.ajax.prototype.commands.userloginAjaxCallback = function(ajax, response, status){
		if (response.selectedValue == 1){
			$( "#user-login .form-item-pass" ).show();
			$( "#user-login .form-actions" ).show();
			$( "#user-login .dummytext_asbton" ).hide();
		} else {
			$( "#user-login .form-item-pass" ).hide();
			$( "#user-login .form-actions" ).hide();
			$( "#user-login .dummytext_asbton" ).show();
		}
	};
}(jQuery, Drupal));