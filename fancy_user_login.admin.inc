<?php

/**
 * @file
 * Node title validation admin file.
 */

/**
 * Implements hook_form().
 */

  function fancy_user_login_admin_form($form, &$form_state) {
	$userlogin_passconfig = $form = array();
	   
	$form['userlogin_settings_configure'] = array(
		'#title' => t('User Login Configuration'),
		'#type' => 'checkbox',
		'#default_value' => variable_get('userlogin_settings_configure' , 0),
	);
    return system_settings_form($form);
}

?>